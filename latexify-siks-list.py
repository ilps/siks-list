#!/usr/bin/env python

import re
import urllib2
import latexcodec
import sys

SOURCE_URL = "http://www.siks.nl/ond/ps/dissertaties.txt"
OUT_FILE = "siks-list.tex"

p_author_uni = re.compile("([^(]+)\s*\(([^)]+)\)")
p_year = re.compile("^\s*(\d{4})\s*$")
p_title = re.compile("Met als ondertitel: |: |; | --- | -- | - ")
p_year_number = re.compile("^\d{4}-\d+")


unimap = {"UVA": "UvA",
          "Uva": "UvA",
          "VU": "VUA",
          "TUE": "TUe"}

author = None
uni = None
year = None

data = urllib2.urlopen(SOURCE_URL).read()
if len(sys.argv) > 1:
    data += "\n" + open(sys.argv[1]).read()
fout = open(OUT_FILE, 'w')

for rawline in data.splitlines():
    line = rawline.decode("Windows-1252")

    try: line.encode("latex")
    except:
        line = line.replace(u'\u201a', u"\u00E9") # FIX FOR 2000-8
        line = line.replace(u'\u2030', u"\u00EB") # FIX FOR 2004-8

    line = re.sub(p_year_number, '', line)
    line = line.strip()

    if line.startswith("Promotor:"):
        continue


    if not line:
        continue
    m = p_year.match(line)
    if m:
        if year:
            print >> fout, "\\end{sikslist}"
        year = m.group(1)
        print >> fout, "\\begin{sikslist}{%s}\n" % year
        continue
    if not author:
        m = p_author_uni.match(line)
        if m:
            author = m.group(1).strip()
            uni = m.group(2).strip()
            if uni in unimap:
                uni = unimap[uni]
            author = author.replace(".", ". ").replace("  ", " ")
    else:
        title = line.rstrip('.')
        split_title = re.split(p_title, title, maxsplit=1)

        if len(split_title) > 1:
            title = "%s: %s" % (split_title[0].strip(" .:,;\""),
                                 split_title[1].strip(" .:,;\""))
        title = title.strip(" .:,;\"")
        print >> fout, ("\siksitem{%s}{%s}{%s}\n" %
                        (author, uni, title)).encode("latex")
        author = None
        uni = None

print >> fout,  "\\end{sikslist}"